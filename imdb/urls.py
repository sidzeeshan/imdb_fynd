from django.conf.urls import include, url
from rest_framework import routers

from .views import MoviesViewSet

router = routers.DefaultRouter()
router.register(r'v1/movies', MoviesViewSet)

urlpatterns = [
    url(r'', include((router.urls, 'imdb'), namespace='imdb'))
]
