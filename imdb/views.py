from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from django_filters.rest_framework import DjangoFilterBackend

from .models import Movie
from .serializers import MovieSerializer
from .filters import MovieFilter


class MoviesViewSet(viewsets.ModelViewSet):

    queryset = Movie.objects.prefetch_related('genre').all()
    serializer_class = MovieSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = MovieFilter
    permission_classes = (IsAdminUser,)

    def get_permissions(self):
        if self.action in ['retrieve', 'list']:
            return [AllowAny(), ]
        return super(MoviesViewSet, self).get_permissions()
