from django.db import models


class TimeStampMixin(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Genre(TimeStampMixin):
    name = models.CharField(max_length=30)

    class Meta:
        db_table = 'genres'
        app_label = "imdb"

    def __str__(self):
        return self.name


class Movie(TimeStampMixin):
    name = models.CharField(max_length=100)
    director = models.CharField(max_length=50)
    imdb_score = models.FloatField(default=0.0)
    popularity99 = models.FloatField(default=0.0)
    genre = models.ManyToManyField('imdb.Genre')

    class Meta:
        db_table = 'movies'
        app_label = "imdb"

    def __str__(self):
        return self.name
