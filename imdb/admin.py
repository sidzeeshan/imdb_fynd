from django.contrib import admin

from .models import Movie, Genre


class MovieAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'director', 'imdb_score', 'popularity99')
    search_fields = ('id', 'name', 'director')
    raw_id_fields = ['genre']
    readonly_fields = ['created_on', 'updated_on']
    list_per_page = 50

admin.site.register(Movie, MovieAdmin)


class GenreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('id', 'name')
    readonly_fields = ['created_on', 'updated_on']
    list_per_page = 50

admin.site.register(Genre, GenreAdmin)
