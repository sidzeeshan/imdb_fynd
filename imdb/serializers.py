from rest_framework import serializers

from .models import Movie, Genre


class GenreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre
        fields = ['id', 'name']
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class MovieSerializer(serializers.ModelSerializer):
    genre = GenreSerializer(many=True)

    class Meta:
        model = Movie
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }

    def create(self, validated_data):
        genres = validated_data.pop('genre')

        movie_obj = Movie.objects.create(**validated_data)
        for genre_dict in genres:
            genre_obj = Genre.objects.filter(name=genre_dict["name"]).first()
            if genre_obj:
                movie_obj.genre.add(genre_obj.id)
        return movie_obj

    def update(self, instance, validated_data):
        genres = validated_data.pop('genre')

        for genre_id in list(instance.genre.all().values_list('id', flat=True)):
            instance.genre.remove(genre_id)

        for genre_dict in genres:
            genre_obj = Genre.objects.filter(name=genre_dict["name"]).first()
            if genre_obj:
                instance.genre.add(genre_obj.id)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return instance
