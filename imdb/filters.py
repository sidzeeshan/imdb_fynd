import django_filters
from django_filters.rest_framework import filters
from dateutil.parser import parse

from .models import Movie


class MovieFilter(django_filters.FilterSet):
    created_on = filters.Filter(method='filter_by_created_on')

    class Meta:
        model = Movie
        fields = "__all__"

    def filter_by_created_on(self, queryset, name, value):
        value = parse(value).date()
        queryset = queryset.filter(created_on__contains=value)
        return queryset
