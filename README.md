# imdb_fynd

Features of Project
1) Django Logging Module along with a Custom Middleware (config/utility/middleware.py) is added to log the incoming requests
   and outgoing responses in config/logs folder.

2) The http://127.0.0.1:8000/api/web/v1/movies/ api is created using DRF and DRF filters
   The admin is authorized to do CRUD transactions while no authentication is required for Retrieve and List requests.
   Use token based authentication to access the api's which require authentication i.e. "Token token-name"

3) Since it uses DRF and DRF filters, the GET api can be used to get single instance (/movies/1/); list of all movies (/movies/);
   filter based on the Movie table columns using query params (/movies/?name=Cabiria), (/movies/?imdb_score=8.3) etc

   Currently, exact match queries are performed but for fields such as name "contains" or "icontains" can be used by
   overriding the respective fields (check imdb/filters.py)

4) Admin can be accessed through http://127.0.0.1:8000/admin (check imdb/admin.py)

5) Tokens creation for the other users can be created using the command -->
   (python manage.py drf_create_token username-of-the user)


Server or Local Machine Setup:
1) Create your own virtual environment and install the basic required packages from config/requirements/production.txt
   or else for local machine development install from the development.txt file

2) Create secrets.json file for your own server, with the changes to values of the keys present in the the file.
{
  "SECRET_KEY": "",
  "DATABASE_HOST": "",
  "DATABASE_USERNAME": "",
  "DATABASE_PASSWORD": ""
}
For e.g. while setting the server for staging (in future if authentication is added), the "DATABASE_PASSWORD" will be the staging DB password
         and for local development it will be your machine's mysql "root" password and so on for other servers and remaining secrets.json keys
         Currently, no need to set the DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD keys

3) Override the DJANGO_SETTINGS_MODULE by exporting it to a settings file. For e.g.
   > For Production Server, export DJANGO_SETTINGS_MODULE='config.settings.production' (To read from the production.py settings file)
   > For Staging Server, export DJANGO_SETTINGS_MODULE='config.settings.staging' (To read from the staging.py settings file)
   > For Local Development, export DJANGO_SETTINGS_MODULE='config.settings.local' (To read from the local.py settings file).
     You can further modify the local settings file as per your needs, by creating your own settings file in
     config/settings/developer_specific/ folder
